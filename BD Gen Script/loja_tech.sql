DROP database IF EXISTS loja_tech;
CREATE DATABASE loja_tech;
USE loja_tech;

DROP TABLE IF EXISTS `loja_tech`.`clientes` ;
CREATE TABLE IF NOT EXISTS `loja_tech`.`clientes` (
  `num_cliente` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(70) NOT NULL,
  `morada` VARCHAR(100) NOT NULL,
  `pontos` INT UNSIGNED NULL,
  `cod_postal` VARCHAR(20) NULL,
  PRIMARY KEY (`num_cliente`)
)ENGINE = InnoDB;

DROP TABLE IF EXISTS `loja_tech`.`fabricantes` ;
CREATE TABLE IF NOT EXISTS `loja_tech`.`fabricantes` (
  `cod_fabricante` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(70) NOT NULL,
  `localizacao` VARCHAR(100) NOT NULL,
  `cod_postal` VARCHAR(20) NULL,
  PRIMARY KEY (`cod_fabricante`)
)ENGINE = InnoDB;

DROP TABLE IF EXISTS `loja_tech`.`categorias` ;
CREATE TABLE IF NOT EXISTS `loja_tech`.`categorias` (
  `categoria` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(1000) NOT NULL,
  PRIMARY KEY (`categoria`)
)ENGINE = InnoDB;

DROP TABLE IF EXISTS `loja_tech`.`produtos` ;
CREATE TABLE IF NOT EXISTS `loja_tech`.`produtos` (
  `cod_produto` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(70) NOT NULL,
  `cod_fabricante` INT UNSIGNED NOT NULL,
  `categoria` INT UNSIGNED NOT NULL,
  `quantidade` INT UNSIGNED NULL,
  `preco` INT UNSIGNED NULL,
  PRIMARY KEY (`cod_produto`),
  FOREIGN KEY (`cod_fabricante`)
  REFERENCES `loja_tech`.`fabricantes` (`cod_fabricante`),
  FOREIGN KEY (`categoria`)
  REFERENCES `loja_tech`.`categorias` (`categoria`)
)ENGINE = InnoDB;

DROP TABLE IF EXISTS `loja_tech`.`vendas` ;
CREATE TABLE IF NOT EXISTS `loja_tech`.`vendas` (
  `num_venda` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `num_cliente` INT UNSIGNED NOT NULL,
  `data_venda` DATE NOT NULL,
  `preco_total` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`num_venda`),
  FOREIGN KEY (`num_cliente`)
  REFERENCES `loja_tech`.`clientes` (`num_cliente`)
)ENGINE = InnoDB;

DROP TABLE IF EXISTS `loja_tech`.`linhas_venda` ;
CREATE TABLE IF NOT EXISTS `loja_tech`.`linhas_venda` (
  `cod_produto` INT UNSIGNED NOT NULL,
  `num_venda` INT UNSIGNED NOT NULL,
  `quantidade` INT UNSIGNED NOT NULL,
  `preço` INT UNSIGNED NOT NULL,
  FOREIGN KEY (`num_venda`)
  REFERENCES `loja_tech`.`vendas` (`num_venda`),
  FOREIGN KEY (`cod_produto`)
  REFERENCES `loja_tech`.`produtos` (`cod_produto`)
)ENGINE = InnoDB;

INSERT	INTO	clientes
VALUES	(NULL, 'Nelson', 'Águeda', 56789, '3750;317'),
	    	(NULL, 'Rodrigues', 'Paredes', 740, '3750;324'),
    		(NULL, 'Francisco', 'Raso de Paredes', 10, '3750;316'),
    		(NULL, 'Sofia', 'Carqueijo', 239, '3750;352'),
    		(NULL, 'Carla', 'Redonda', 4122, '3750;378'),
    		(NULL, 'Carlos', 'Espinhel', 3123, '3750;403'),
	    	(NULL, 'Rita', 'Lamas do Vouga', 21, '3750;551'),
	    	(NULL, 'Sergio Fonseca', 'Urgueira', 34, '3750;562'),
	    	(NULL, 'Lisete Matos', 'Barrosa', 2346, '3750;671'),
	    	(NULL, 'Rodrigo Mendes', 'Chousinha', 23423, '3750;711'),
        (NULL, 'Sofia Brito', 'Ferreirós', 12323, '3750;716'),
        (NULL, 'Rogério', 'Póvoa Carvalha', 52346, '3750;720'),
        (NULL, 'Sergio Artur', 'Fontinha', 423, '3750;741'),
        (NULL, 'Julio Martins', 'Trofa', 230 , '3750;791'),
        (NULL, 'Adália', 'Aguieira', 51235, '3750;879'),
        (NULL, 'Açucena', 'Moitedo', 5123, '3750;825'),
        (NULL, 'Abrão', 'Sabugal', 623, '3750;831'),
        (NULL, 'Ália', 'Toural', 83457, '3750;835'),
        (NULL, 'Adriano Mendes', 'Valongo do Vouga', 6234, '3750;836'),
        (NULL, 'Carlos Adilio', 'Cavadas', 62134, '3750;326'),
        (NULL, 'Aldara', 'Carvoeiro', 6235, '3750;603'),
        (NULL, 'Alexandre', 'Aldeia', 52345, '3750;743'),
        (NULL, 'Alcindo', 'Felgueira', 7345, '3750;020'),
        (NULL, 'André Ferreira', 'Boa Aldeia', 62341, '3750;021'),
        (NULL, 'Bernardo', 'Crasto São Jorge', 23235, '3750;713'),
        (NULL, 'Belina Cárin', 'Cumeada', 5234, '3750;881'),
        (NULL, 'Berto Canto', 'Sobreiro', 63456, '3850;273'),
        (NULL, 'Belisa Clicia', 'Alquerubim', 52345, '3850;336'),
        (NULL, 'Benicio Celsa', 'Angeja', 622, '3850;440'),
        (NULL, 'Caetano Rodrigues', 'Boavista', 62345, '3850;480'),
        (NULL, 'Camilia Caia', 'Branca', 6236, '3850;533'),
        (NULL, 'Cid', 'Cristelo', 7345, '3850;567'),
        (NULL, 'Dilan', 'Palhal', 412, '3850;582'),
        (NULL, 'Donzilio', 'Frossos', 5123, '3850;635'),
        (NULL, 'Élder', 'Alto dos Barreiros', 6234, '3850;701'),
        (NULL, 'Egil Eduino', 'Loure', 5252, '3850;781'),
        (NULL, 'Estela', 'Pinheiro', 5123, '3850;731'),
        (NULL, 'Fábio Sousa', 'Valmaior', 1234, '3850;835'),
        (NULL, 'Frederico', 'Aguim', 62345, '3780;636'),
        (NULL, 'Giana', 'Amoreira da Gândara', 423, '3780;011'),
        (NULL, 'Gislene', 'Madureirinha', 512, '3780;014'),
        (NULL, 'Helda', 'Alféloas', 10, '3780;313'),
        (NULL, 'Heliana', 'Anadia', 512, '3780;242'),
        (NULL, 'Isália Ingue', 'Avelãs de Caminho', 623, '3780;362'),
        (NULL, 'Ismael', 'Candeeira', 470, '3780;403'),
        (NULL, 'Joaquim Sousa', 'Mogofores', 623, '3780;453'),
        (NULL, 'José Jair', 'Moita', 722, '3780;476'),
        (NULL, 'Jonas Rodrigues', '', 76234, ''),
        (NULL, 'Jaques', 'Óis do Bairro', 623, '3780;505'),
        (NULL, 'Jasão', 'Póvoa da Palmeira', 523, '3780;525'),
        (NULL, 'Júlia Joice', 'Ribeiro', 4123, '3780;529'),
        (NULL, 'Karen', 'Sangalhos', 346, '3780;135'),
        (NULL, 'Karina', 'Vidoeiro', 2356, '3780;150'),
        (NULL, 'Luis Laurentino', 'Espairo', 734, '3780;173'),
        (NULL, 'Laurentina', 'Pedralva', 242, '3780;177'),
        (NULL, 'Lourenço', 'São Lourenço do Bairro', 52, '3780;192'),
        (NULL, 'Leo', 'Mata da Curia',512 , '3780;551'),
        (NULL, 'Luciana', 'Monsarros',63 , '3780;569'),
        (NULL, 'Leonel', 'Poutena', 345, '3780;602'),
        (NULL, 'Melinda', 'Samel', 8433, '3780;596'),
        (NULL, 'Marco', 'Vilarinho do Bairro', 2734, '3780;599'),
        (NULL, 'Marcelo', 'Moção', 8567, '4540;565'),
        (NULL, 'Mariano', 'Reguengo', 23743, '4540;670'),
        (NULL, 'Melissa', 'Barroco', 2347, '4540;203'),
        (NULL, 'Óscar', 'Fonte', 2385, '4540;507'),
        (NULL, 'Rafael', 'Crasto', 356, '4540;761'),
        (NULL, 'Ricardo', 'Calvário', 8543, '4540;327'),
        (NULL, 'Rita Sousa', 'Bouças', 635467, '4540;611'),
        (NULL, 'Renato', 'Covela', 3456, '4540;451'),
        (NULL, 'Russel', 'Vila Cova', 3458, '4540;441'),
        (NULL, 'Rui Pinto', 'Ilha Vedra', 1236, '4540;461'),
        (NULL, 'Safia', 'Seixo', 67956, '4540;037'),
        (NULL, 'Salazar Saul', 'Sorregos', 2348, '4540;061'),
        (NULL, 'Samuel Sancler', 'Mirante', 23489, '4540;576'),
        (NULL, 'Sandra', 'Castanheira', 458796, '4540;012'),
        (NULL, 'Solana', 'Lourido', 2347, '4540;036'),
        (NULL, 'Tália', 'Vale de Froias', 4542, '4540;051'),
        (NULL, 'Tatiana', 'Arouca', 2348, '4540;099'),
        (NULL, 'Tiago', 'Sardoal', 2348, '4540;225'),
        (NULL, 'Ticiano', 'Farrapa', 3458, '4540;998'),
        (NULL, 'Vasco', 'Alvite de Cima', 239645, '4540;295'),
        (NULL, 'Vanessa', 'Vale de Lameiro', 21346, '4540;317'),
        (NULL, 'Vitor', 'Ponta do Rêgo', 3943, '4540;346'),
        (NULL, 'Xavier', 'Vila Viçosa', 21976, '4540;349'),
        (NULL, 'Xico', 'Paramo', 2348, '4540;381'),
        (NULL, 'Jabob', 'Cruzeiro', 234754, '4540;442');
    
INSERT	INTO	fabricantes
VALUES  (NULL, 'Mitsubishi Electric', 'Avenida do Forte n. 10, Carnaxide, Lisboa, Portugal', '2794-019'),
        (NULL, 'Varta', 'Ellwangen, Germany', '5123-512'),
        (NULL, 'Wacom', 'Kazo, Saitama, Japan', '347-0007'),
        (NULL, 'Blaupunkt', 'Hildesheim, Germany', '3434-6124'),
        (NULL, 'Asus', '	Beitou District, Taipei, Taiwan', '23-62344'),
        (NULL, 'Bosch', 'Gerlingen, Germany', '512-52'),
        (NULL, 'Philips', 'Amsterdam, Netherlands', '133-5134'),
        (NULL, 'Microsoft', 'Redmond, Washington, United States', '512-523'),
        (NULL, 'Canon', 'Ota, Tokyo, Japan', '612-1');
        
INSERT	INTO	categorias
VALUES  (NULL, 'Electrodomesticos'),
        (NULL, 'Som e Imagem'),
        (NULL, 'Informática Desktop'),
        (NULL, 'Informatica Laptop'),
        (NULL, 'Informatia Perifericos'),
        (NULL, 'Acessorios'),
        (NULL, 'Auto'),
        (NULL, 'Grandes Domesticos'),
        (NULL, 'Pequenos Domesticos'),
        (NULL, 'Iluminação'),
        (NULL, 'Jogos e Consolas');
        
#cod_produto, nome, cod_fabricante, categoria, quantidade, preco
INSERT INTO produtos
VALUES  (NULL, 'Ar Condicionado', 1, 8, 10, 700),
        (NULL, 'Pilhas AA', 2, 6, 120, 2),
        (NULL, 'Intuos Pro Medium', 3, 5, 8, 300),
        (NULL, 'Toronto 440BT Bluetooth', 4, 7, 20, 60),
        (NULL, 'Blaupunkt BPA-695 earbuds', 4, 5, 30, 15),
        (NULL, 'Asus ROG G20CB-76D9 Desktop', 5, 3, 2, 1600),
        (NULL, 'CERBERUS GAMING MOUSE', 5, 5, 10, 30),
        (NULL, 'ECHELON LASER GAMING MOUSE', 5, 5, 10, 43),
        (NULL, 'GX950 LASER GAMING MOUSE', 5, 5, 10, 44),
        (NULL, 'OG SWIFT PG278Q GAMING MONITOR ', 5, 5, 5, 720),
        (NULL, 'PORTATIL ASUS A540LA', 5, 4, 8, 425),
        (NULL, 'PORTATIL ASUS E403SA', 5, 4, 9, 470),
        (NULL, 'PORTATIL ASUS P2420LJ', 5, 4, 3, 677),
        (NULL, 'Bosch SMS41D08EU', 6, 8, 5, 370),
        (NULL, 'Bosch KDV 29VW30', 6, 8, 2, 284),
        (NULL, 'Bosch PHD 2511B', 6, 9, 16, 10),
        (NULL, 'Philips 22PFH4000/88', 7, 2, 7, 135),
        (NULL, 'Lâmpada LED E27 13,5W', 7, 10, 40, 10),
        (NULL, 'Xbox One', 8, 11, 8, 282),
        (NULL, 'Xbox One Wireless Gamepad', 8, 11, 20, 62),
        (NULL, 'EOS M3 Black', 9, 2, 3, 535);
        
#num_venda, num_cliente, data_venda, preco_total
INSERT INTO vendas
VALUES  (NULL, 1, STR_TO_DATE('01-01-2016', '%d-%m-%Y'), 704),
        (NULL, 5, STR_TO_DATE('16-01-2016', '%d-%m-%Y'), 445),
        (NULL, 2, STR_TO_DATE('25-01-2016', '%d-%m-%Y'), 70),
        (NULL, 15, STR_TO_DATE('08-01-2016', '%d-%m-%Y'), 60),
        (NULL, 25, STR_TO_DATE('10-02-2016', '%d-%m-%Y'), 832),
        (NULL, 15, STR_TO_DATE('26-03-2016', '%d-%m-%Y'), 1600),
        (NULL, 12, STR_TO_DATE('01-01-2016', '%d-%m-%Y'), 60),
        (NULL, 8, STR_TO_DATE('27-04-2016', '%d-%m-%Y'), 300),
        (NULL, 15, STR_TO_DATE('27-04-2016', '%d-%m-%Y'), 43),
        (NULL, 2, STR_TO_DATE('27-04-2016', '%d-%m-%Y'), 740),
        (NULL, 5, STR_TO_DATE('01-01-2016', '%d-%m-%Y'), 677),
        (NULL, 7, STR_TO_DATE('27-04-2016', '%d-%m-%Y'), 470),
        (NULL, 2, STR_TO_DATE('27-04-2016', '%d-%m-%Y'), 425),
        (NULL, 19, STR_TO_DATE('01-01-2016', '%d-%m-%Y'), 1100),
        (NULL, 22, STR_TO_DATE('27-04-2016', '%d-%m-%Y'), 660),
        (NULL, 35, STR_TO_DATE('27-04-2016', '%d-%m-%Y'), 1429);

#cod_produto, num_venda, quantidade, preco_total
INSERT INTO linhas_venda
VALUES (1, 1, 1, 700),
       (2, 1, 1, 2),
       (2, 1, 1, 2),
       (3, 2, 1, 300),
       (4, 3, 1, 60),
       (4, 4, 1, 60),
       (5, 5, 1, 15),
       (6, 6, 1, 1600),
       (4, 7, 1, 60),
       (3, 8, 1, 300),
       (8, 9, 1, 43),
       (14, 10, 2, 740),
       (13, 11, 1, 677),
       (12, 12, 1, 470),
       (11, 13, 1, 425),
       (10, 14, 1, 720),
       (9, 15, 15, 660),
       (7, 16, 10, 300),
       (8, 16, 1, 43),
       (14, 16, 2, 740),
       (15, 16, 1, 284),
       (16, 14, 1, 10),
       (16, 2, 1, 10),
       (17, 2, 1, 135),
       (18, 3, 1, 10),
       (19, 5, 1, 282),
       (20, 16, 1, 62),
       (21, 5, 1, 535),
       (14, 14, 1, 370);        
        
       
#views para fabricante com maior saida  
DROP VIEW IF EXISTS ProdutosPorQuantidadeVendida;
CREATE VIEW ProdutosPorQuantidadeVendida (cod_produto, quantidade)
AS(
  SELECT DISTINCT (cod_produto), SUM(quantidade)
  FROM linhas_venda
  GROUP BY cod_produto
  ORDER BY SUM(quantidade) DESC
);

DROP VIEW IF EXISTS ProdutoMaisVendido;
CREATE VIEW ProdutoMaisVendido (cod_produto, quantidade)
AS(
  SELECT cod_produto, quantidade
  FROM ProdutosPorQuantidadeVendida
  WHERE quantidade IN (
      SELECT max(quantidade)
      FROM ProdutosPorQuantidadeVendida
  )
);

#Fabricante do produto mais vendido
DROP VIEW IF EXISTS FabricanteMaiorSaida;
CREATE VIEW FabricanteMaiorSaida (cod_fabricante, nome)
AS(
  SELECT F.cod_fabricante, F.nome
  FROM fabricantes F, produtos P, ProdutoMaisVendido PMV
  WHERE PMV.cod_produto = P.cod_produto AND P.cod_fabricante = F.cod_fabricante
);
       

#views para os 3 melhores clientes por quantidade comprada
DROP VIEW IF EXISTS ComprasPorCliente;
CREATE VIEW ComprasPorCliente (num_cliente, nome, quantidade)
AS(
  SELECT DISTINCT (V.num_cliente), C.nome, SUM(LV.quantidade)
  FROM vendas V, linhas_venda LV, clientes C
  WHERE LV.num_venda = V.num_venda AND C.num_cliente = V.num_cliente
  group by V.num_cliente
  order by SUM(LV.quantidade) DESC
);

DROP VIEW IF EXISTS TresMelhoresClientesPorQuantiaComprada;
CREATE VIEW TresMelhoresClientesPorQuantiaComprada (num_cliente, nome, quantidade)
AS(
  SELECT *
  FROM(
      SELECT num_cliente, nome, quantidade
      FROM ComprasPorCliente
      ORDER BY quantidade DESC
      LIMIT 3
  ) AS tmp
  ORDER BY quantidade ASC
);

#views para os 3 melhores clientes por quantia monetária gasta
DROP VIEW IF EXISTS GastosPorCliente;
CREATE VIEW GastosPorCliente (num_cliente, nome, gastos)
AS(
  SELECT DISTINCT (V.num_cliente), C.nome, SUM(V.preco_total)
  FROM vendas V, clientes C
  WHERE C.num_cliente = V.num_cliente
  GROUP BY V.num_cliente
  ORDER BY SUM(V.preco_total) DESC
);

DROP VIEW IF EXISTS TresMelhoresClientes;
CREATE VIEW TresMelhoresClientes (num_cliente, nome, gastos)
AS(
  SELECT *
  FROM(
      SELECT num_cliente, nome, gastos
      FROM GastosPorCliente
      ORDER BY gastos DESC
      LIMIT 3
  ) AS tmp
  ORDER BY gastos DESC
);

#views para os 3 melhores dias de negocio
DROP VIEW IF EXISTS NegociosPorDia;
CREATE VIEW NegociosPorDia (data_venda, num_vendas)
AS(
  SELECT DISTINCT (V.data_venda), count(LV.num_venda)
  FROM vendas V, linhas_venda LV
  WHERE LV.num_venda = V.num_venda
  group by V.data_venda
  order by count(LV.num_venda) DESC
);

DROP VIEW IF EXISTS TresMelhoresDias;
CREATE VIEW TresMelhoresDias (data_venda, num_vendas)
AS(
  SELECT *
  FROM(
    SELECT data_venda, num_vendas
    FROM NegociosPorDia
    ORDER BY num_vendas DESC
    LIMIT 3
  ) AS tmp
  ORDER BY num_vendas DESC
);
 
 

DROP PROCEDURE IF EXISTS inserirVenda;   
DELIMITER @
#create insertion procedure    
CREATE PROCEDURE inserirVenda (IN in_numero_cliente INT UNSIGNED, IN in_preco_total INT UNSIGNED)
BEGIN
  INSERT INTO vendas
  VALUES (NULL, in_numero_cliente, CURDATE(), in_preco_total);
END@

DELIMITER ;


DROP FUNCTION IF EXISTS ultimaVenda;
DELIMITER @
#função retorna ultima venda
CREATE FUNCTION ultimaVenda()
RETURNS INT UNSIGNED
BEGIN
  DECLARE ultimalinhavenda INT UNSIGNED;

  #fetch da ultima venda
  SELECT num_venda
  INTO ultimalinhavenda
  FROM vendas
  ORDER BY num_venda DESC LIMIT 1;
  
  RETURN ultimalinhavenda;
END@

DELIMITER ;



DROP FUNCTION IF EXISTS getStock;
DELIMITER @
#função retorna ultima venda
CREATE FUNCTION getStock(in_cod_produto INT UNSIGNED)
RETURNS INT UNSIGNED
BEGIN
  DECLARE stock INT UNSIGNED;

  #fetch da ultima venda
  SELECT quantidade
  INTO stock
  FROM produtos
  WHERE cod_produto = in_cod_produto;  
  
  RETURN stock;
END@

DELIMITER ;




DROP PROCEDURE IF EXISTS removerUltimaVenda;   
DELIMITER @
#create insertion procedure    
CREATE PROCEDURE removerUltimaVenda()
BEGIN
  DECLARE ultima_venda INT UNSIGNED;
  
  #obter indice ultima venda
  SELECT ultimaVenda()
  INTO ultima_venda;
  
  #remover venda
  DELETE FROM vendas WHERE num_venda = ultima_venda;
END@

DELIMITER ;



DROP PROCEDURE IF EXISTS inserirLinhasVenda;
DELIMITER @
#create insertion procedure       
CREATE PROCEDURE inserirLinhasVenda (IN in_cod_produto INT UNSIGNED, IN in_quantidade_vendida INT UNSIGNED, IN in_preco INT UNSIGNED)
BEGIN
  DECLARE ultima_venda INT UNSIGNED;
  DECLARE stock INT UNSIGNED;
  
  #obter indice ultima venda
  SELECT ultimaVenda()
  INTO ultima_venda;
  
  #inserir linhas de venda
  INSERT INTO linhas_venda
  VALUES (in_cod_produto, ultima_venda, in_quantidade_vendida, in_preco);
    
  #procurar stock produto  
  SELECT getStock(in_cod_produto)
  INTO stock;  
    
  UPDATE produtos SET quantidade = (stock - in_quantidade_vendida) WHERE cod_produto = in_cod_produto;
END@

DELIMITER ;



DROP PROCEDURE IF EXISTS reporStock;
DELIMITER @
#create insertion procedure    
CREATE PROCEDURE reporStock (IN in_cod_produto INT UNSIGNED, IN in_quantidade INT UNSIGNED, IN in_preco INT UNSIGNED)
BEGIN
  DECLARE stock INT UNSIGNED;

  #procurar stock produto  
  SELECT getStock(in_cod_produto)
  INTO stock; 
  
  UPDATE produtos SET quantidade = (stock + in_quantidade) WHERE cod_produto = in_cod_produto;
  UPDATE produtos SET preco = in_preco WHERE cod_produto = in_cod_produto;
END@

DELIMITER ;



#trigger to apply discount on end price after insertion
DELIMITER @

DROP TRIGGER IF EXISTS calcDesconto;
CREATE TRIGGER calcDesconto
BEFORE INSERT ON vendas
FOR EACH ROW BEGIN
    DECLARE pontos_trigger INT UNSIGNED;
    DECLARE preco_tigger INT UNSIGNED;
    
    #fetch pontos desse cliente
    SELECT pontos
    INTO pontos_trigger
    FROM clientes
    WHERE num_cliente = new.num_cliente;
    
    #calcular os novos pontos que o cliente obtem com essa compra
    #por cada 10€ gastos o cliente ganha 5 pontos
    SET pontos_trigger = pontos_trigger + ( ( new.preco_total * 5) / 10 );
    
    #desconto: por cada 100 pontos -> 0.05 (5%) de desconto
    #desconto maximo 40%
    IF pontos_trigger > 800 THEN
      SET new.preco_total = new.preco_total - ( new.preco_total * 0.4 );    
      SET pontos_trigger = pontos_trigger - 800;
    ELSE
      IF pontos_trigger >= 50 THEN #cliente precisa de ter pelo menos 50 pontos para obter desconto
        SET new.preco_total =  new.preco_total - ( new.preco_total * ( ( pontos_trigger * 0.05 ) / 0.04 ) );  
        SET pontos_trigger = 0;
      END IF;
    END IF;

    UPDATE clientes SET pontos = pontos_trigger WHERE num_cliente = new.num_cliente;
END@

DELIMITER ;
