#ifndef __MYSQL_H__
#define __MYSQL_H__

#include <QApplication>
#include <QtSql>
#include <QtSql/QSql>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlDriver>
#include <QDebug>
#include <QWidget>

#define     DATABASE_TYPE    "QMYSQL"
#define     DATABASE_NAME    "loja_tech"
#define     HOST        "192.168.56.101"
#define     DATABASE    "loja_tech"
#define     USER        "root"
#define     PASSWORD    ""
#define     PORT        3306

#endif
