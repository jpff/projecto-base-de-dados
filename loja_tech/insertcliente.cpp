#include "insertcliente.h"
#include "ui_insertcliente.h"

insertCliente::insertCliente(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::insertCliente)
{
    ui->setupUi(this);
    ui->alertFoundMatch->hide();
}

insertCliente::~insertCliente()
{
    delete ui;
}

bool insertCliente::setDatabase(QSqlDatabase* database)
{
    db = database;

    if(db->isOpen()){
        return 1;
    } else {
        return 0;
    }
}

void insertCliente::on_insertClienteButton_accepted()
{
    QString _NomeCliente, _MoradaCliente, _CodigoPostal;
    int _num_cliente;
    bool greenlightFlag = false;    //validation flag
    QSqlQuery* qry = new QSqlQuery(DATABASE_NAME);

    qDebug() << "Status of foundMatch is: " << foundMatch << endl;

    if(foundMatch){ //if a match exists, we're updating a client's information
        qDebug() << "Entered UPDATING process" << endl;
        greenlight greenlightWindow;

        //get modified values
        _NomeCliente = ui->NomeCliente->toPlainText();
        _MoradaCliente = ui->MoradaCliente->toPlainText();
        _CodigoPostal = ui->CodigoPostalCliente->toPlainText();
        _num_cliente = ui->CodigoCliente->toPlainText().toInt();

        qDebug() << "Current _NomeCliente to be updated is: " << _NomeCliente << endl;
        qDebug() << "Current _MoradaCliente to be updated is: " << _MoradaCliente << endl;
        qDebug() << "Current _CodigoPostal to be updated is: " << _CodigoPostal << endl;
        qDebug() << "Current _num_cliente to be updated is: " << _num_cliente << endl;


        //validade that operator wants to update entry values
        qDebug() << "Asking user for confirmation..." << endl;
        greenlightWindow.setFlag(&greenlightFlag);
        greenlightWindow.exec();

        qDebug() << "Processing confirmation..." << endl;
        if(!greenlightFlag){    //if was declined
            ui->NomeCliente->clear();
            ui->MoradaCliente->clear();
            ui->CodigoCliente->clear();
            ui->CodigoPostalCliente->clear();
            _NomeCliente.clear();
            _MoradaCliente.clear();
            _CodigoPostal.clear();
            _num_cliente = 0;

            qDebug() << "Permission denied" << endl;

            return;
        } else {
            qDebug() << "Permission granted!" << endl;
            qDebug() << "Preparing query" << endl;

            //update entry
            qry->prepare("UPDATE clientes SET nome = :in_nome_cliente, morada = :in_morada_cliente, cod_postal = :in_cod_postal WHERE num_cliente = :in_num_cliente");
            qry->bindValue(":in_nome_cliente", _NomeCliente);
            qry->bindValue(":in_morada_cliente", _MoradaCliente);
            qry->bindValue(":in_cod_postal", _CodigoPostal);
            qry->bindValue(":in_num_cliente", _num_cliente);

            if (!qry->exec()){
                qDebug() << "Query report: " << qry->lastError().text() << endl;
            }
            qDebug() << "Completed UPDATE action" << endl;
        }
    } else {    //otherwise we're inserting a new client
        qDebug() << "Entered INSERTION process" << endl;
        _NomeCliente = ui->NomeCliente->toPlainText();
        _MoradaCliente = ui->MoradaCliente->toPlainText();
        _CodigoPostal = ui->CodigoPostalCliente->toPlainText();

        qDebug() << "Current _NomeCliente is: " << _NomeCliente << endl;
        qDebug() << "Current _MoradaCliente is: " << _MoradaCliente << endl;
        qDebug() << "Current _CodigoPostal is: " << _CodigoPostal << endl;

        ui->CodigoCliente->clear();

        qDebug() << "Preparing query" << endl;
        qry->prepare("INSERT INTO clientes (num_cliente, nome, morada, pontos, cod_postal) VALUES (NULL, :in_nome_cliente, :in_morada_cliente, 0, :in_cod_postal)");
        qry->bindValue(":in_nome_cliente", _NomeCliente);
        qry->bindValue(":in_morada_cliente", _MoradaCliente);
        qry->bindValue(":in_cod_postal", _CodigoPostal);

        if (!qry->exec()){
            qDebug() << "Query report: " << qry->lastError().text() << endl;
        }
        qDebug() << "Completed INSERT action" << endl;
    }

    greenlightFlag = false;

    qDebug() << "Reached ending for client insertions" << endl;

    qDebug() << "Greenlight flag final status is:" << greenlightFlag << endl;

    return;
}

void insertCliente::on_insertClienteButton_rejected()
{
    close();

    return;
}

void insertCliente::on_CodigoCliente_textChanged()
{
    QSqlQuery* qry = new QSqlQuery(DATABASE_NAME);
    volatile int _num_cliente = -1; //it's important that _num_cliente is -1 before any of the following instructions
                                    //are executed
                                    //compiler must not optimize anything using this variable

    _num_cliente = (ui->CodigoCliente->toPlainText()).toInt();  //read input value
    qDebug() << "Inserted value is: " << _num_cliente << endl;

    //search records for that value
    qry->prepare("select * from clientes where num_cliente = :in_num_cliente");
    qry->bindValue(":in_num_cliente", _num_cliente);

    if (!qry->exec()){
        qDebug() << "Query report: " << qry->lastError().text() << endl;
    }

    qDebug() << "Found" << qry->record().count() << "entries." << endl;

    //process entries
    if(qry->size() > 0 && _num_cliente > -1){ //if found a match then fill in all the other fields for the user
        if(qry->size() > 1){
            qDebug() << "Query found more than one client for the same primary key!" << endl;
            qDebug() << "Will ignore and return without further changes!" << endl;

            return;
        }
        qry->next();    //set query pointer to the first, and only row found (only one primary key per client)
        qDebug() << "Size of query reports: " << qry->size() << endl;
        foundMatch = true;

        ui->alertFoundMatch->show();

        qDebug() << "NomeCliente found was " << qry->value(1).toString() << endl;
        qDebug() << "MoradaCliente found was " << qry->value(2).toString() << endl;
        qDebug() << "CodigoPostalCliente found was " << qry->value(4).toString() << endl;

        ui->NomeCliente->clear();
        ui->MoradaCliente->clear();
        ui->CodigoPostalCliente->clear();
        ui->NomeCliente->appendPlainText((qry->value(1)).toString());
        ui->MoradaCliente->appendPlainText((qry->value(2)).toString());
        ui->CodigoPostalCliente->appendPlainText((qry->value(4)).toString());
    } else {    //if no match found clear the other fields
        foundMatch = false;
        ui->NomeCliente->clear();
        ui->MoradaCliente->clear();
        ui->CodigoPostalCliente->clear();
        ui->alertFoundMatch->hide();
    }

    qDebug() << "foundMatch = " << foundMatch << endl;

    return;
}
