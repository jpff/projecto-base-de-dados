#include "insertcategorias.h"
#include "ui_insertcategorias.h"

insertCategorias::insertCategorias(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::insertCategorias)
{
    ui->setupUi(this);
    ui->alertFoundMatch->hide();
}

insertCategorias::~insertCategorias()
{
    delete ui;
}

bool insertCategorias::setDatabase(QSqlDatabase* database)
{
    db = database;

    if(db->isOpen()){
        return 1;
    } else {
        return 0;
    }
}

void insertCategorias::on_buttonBox_accepted()
{
    QString _descricaoCategoria;
    QSqlQuery* qry = new QSqlQuery(DATABASE_NAME);

    qDebug() << "Status of foundMatch is: " << foundMatch << endl;

    if(foundMatch){ //if a match exists, we're updating a client's information
        qDebug() << "An entry for that category already exists" << endl;

        return;
    } else {
        qDebug() << "Entered INSERTION process" << endl;
        _descricaoCategoria = ui->descricaoCategoria->toPlainText();

        qDebug() << "Current _descricaoCategoria is: " << _descricaoCategoria << endl;

        ui->descricaoCategoria->clear();

        qDebug() << "Preparing query" << endl;
        qry->prepare("INSERT INTO categorias (categoria, descricao) VALUES (NULL, :in_descricao)");
        qry->bindValue(":in_descricao", _descricaoCategoria);

        if (!qry->exec()){
            qDebug() << "Query report: " << qry->lastError().text() << endl;
        }
        qDebug() << "Completed INSERT action" << endl;
    }

    qDebug() << "Reached ending for category insertions" << endl;

    return;
}

void insertCategorias::on_buttonBox_rejected()
{
    close();

    return;
}

void insertCategorias::on_descricaoCategoria_textChanged()
{
    QSqlQuery* qry = new QSqlQuery(DATABASE_NAME);
    QString _descricaoCategoria;

    _descricaoCategoria = ui->descricaoCategoria->toPlainText();  //read input value
    qDebug() << "Inserted value is: " << _descricaoCategoria << endl;

    //search records for that value
    qry->prepare("select * from categorias where descricao = :in_descricao");
    qry->bindValue(":in_descricao", _descricaoCategoria);
    qry->exec();

    qDebug() << "Possible errors: " << qry->lastError() << endl;
    qDebug() << "Found" << qry->record().count() << "entries." << endl;

    //process entries
    if(qry->size() > 0){ //if found a match then fill in all the other fields for the user
        qDebug() << "Category already exists!" << endl;
        qDebug() << "Size of query reports: " << qry->size() << endl;
        foundMatch = true;
        ui->alertFoundMatch->show();

        qDebug() << "Categoria found was " << qry->value(1).toString() << endl;
    } else {    //if no match found clear the other fields
        foundMatch = false;
        ui->alertFoundMatch->hide();
    }

    qDebug() << "foundMatch = " << foundMatch << endl;

    return;
}
