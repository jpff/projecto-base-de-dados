#include "startupdialog.h"
#include "ui_startupdialog.h"

startupDialog::startupDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::startupDialog)
{
    ui->setupUi(this);
}

startupDialog::~startupDialog()
{
    delete ui;
}

void startupDialog::on_pushButton_clicked()
{
    close();

    return;
}

void startupDialog::outputLine(QString line)
{
    ui->startupEcho->isReadOnly();
    ui->startupEcho->append(line);

    return;
}
