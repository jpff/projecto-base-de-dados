#ifndef INSERTPRODUTOS_H
#define INSERTPRODUTOS_H

#include <QDialog>
#include <QtSql>
#include "mysql.h"
#include "greenlight.h"

namespace Ui {
class insertProdutos;
}

class insertProdutos : public QDialog
{
    Q_OBJECT

public:
    explicit insertProdutos(QWidget *parent = 0);
    ~insertProdutos();
    bool setDatabase(QSqlDatabase*);

private slots:
    void on_insertProdutosButton_accepted();

    void on_insertProdutosButton_rejected();

    void on_NomeProduto_textChanged();

    void on_CodigoProduto_textChanged();

private:
    Ui::insertProdutos* ui;
    QSqlDatabase* db;
    bool foundMatch = false;
    bool latch = false;
};

#endif // INSERTPRODUTOS_H
