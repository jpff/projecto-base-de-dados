#include "insertprodutos.h"
#include "ui_insertprodutos.h"

insertProdutos::insertProdutos(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::insertProdutos)
{
    ui->setupUi(this);
    ui->alertCodigoProduto->hide();
    ui->alertNomeProduto->hide();
}

insertProdutos::~insertProdutos()
{
    delete ui;
}

bool insertProdutos::setDatabase(QSqlDatabase* database)
{
    db = database;

    if(db->isOpen()){
        return 1;
    } else {
        return 0;
    }

    on_CodigoProduto_textChanged();
    on_NomeProduto_textChanged();
}

void insertProdutos::on_insertProdutosButton_accepted()
{
    qDebug() << "@on_insertProdutosButton_accepted()" << endl;
    QSqlQuery* qry = new QSqlQuery;
    QString _nome_produto, _codigo_produto, _quantidade, _cod_fabricante, _fabricante_nome, _cod_categoria, _categoria_descricao, _preco_produto;

    //fetch values from GUI
    _nome_produto = ui->NomeProduto->toPlainText();
    _codigo_produto = ui->CodigoProduto->toPlainText();
    _preco_produto = ui->precoProduto->toPlainText();
    _quantidade = ui->QuantidadeProduto->toPlainText();
    _fabricante_nome = ui->FabricanteProdutoComboBox->currentText();
    _categoria_descricao = ui->CategoriaProdutoComboBox->currentText();

    //get values of _cod_fabricante and _cod_categoria
    qry->clear();
    qry->prepare("SELECT cod_fabricante FROM fabricantes WHERE nome = :in_nome_fabricante");
    qry->bindValue(":in_nome_fabricante", _fabricante_nome);
    if (!qry->exec()){
        qDebug() << "Query report: " << qry->lastError().text() << endl;
    }
    qry->next();
    _cod_fabricante = qry->value(0).toString();

    qry->clear();
    qry->prepare("SELECT categoria FROM categorias WHERE descricao = :in_categoria_descricao");
    qry->bindValue(":in_categoria_descricao", _categoria_descricao);
    if (!qry->exec()){
        qDebug() << "Query report: " << qry->lastError().text() << endl;
    }
    qry->next();
    _cod_categoria = qry->value(0).toString();

    if(foundMatch){ //then we're updating registries
        //repor stock para produto base de dados
        //update registries in database
        qry->clear();
        qry->prepare("call reporStock(:in_cod_produto, :in_quantidade, :in_preco)");
        qry->bindValue(":in_cod_produto", _codigo_produto.toInt());
        qry->bindValue(":in_quantidade", _quantidade.toInt());
        qry->bindValue(":in_preco", _preco_produto);
        if (!qry->exec()){
            qDebug() << "Query report: " << qry->lastError().text() << endl;
        }

        return;
    } else {    //then we're inserting a new value
        qry->clear();
        qry->prepare("INSERT INTO produtos VALUES (NULL, :in_nome_produto, :in_cod_fabricante, :in_cod_categoria, :in_quantidade, :in_preco)");
        qry->bindValue(":in_nome_produto", _nome_produto);
        qry->bindValue(":in_cod_fabricante", _cod_fabricante.toInt());
        qry->bindValue(":in_cod_categoria", _cod_categoria.toInt());
        qry->bindValue(":in_quantidade", _quantidade.toInt());
        qry->bindValue(":in_preco", _preco_produto.toInt());
        if (!qry->exec()){
            qDebug() << "Query report: " << qry->lastError().text() << endl;
        }

        return;
    }
}

void insertProdutos::on_insertProdutosButton_rejected()
{
    close();

    return;
}

void insertProdutos::on_NomeProduto_textChanged()
{
    qDebug() << "@on_NomeProduto_textChanged" << endl;
    if(latch){  //to prevent infinite looping between on_CodigoProduto_textChanged() and on_NomeProduto_textChanged() EventHandlers
        latch = false;

        return;
    } else {
        QString _nome_produto;
        QSqlQuery* qry = new QSqlQuery;

        QSqlQueryModel* modal_categorias = new QSqlQueryModel;
        QSqlQueryModel* modal_fabricantes = new QSqlQueryModel;

        //fill in fabricantes combobox
        qry->prepare("SELECT nome FROM fabricantes");

        if (!qry->exec()){
            qDebug() << "Query report: " << qry->lastError().text() << endl;
        }

        modal_fabricantes->setQuery(*qry);

        ui->FabricanteProdutoComboBox->setModel(modal_fabricantes);

        //fill in categorias combobox
        qry->prepare("SELECT descricao FROM categorias");

        if (!qry->exec()){
            qDebug() << "Query report: " << qry->lastError().text() << endl;
        }

        modal_categorias->setQuery(*qry);

        ui->CategoriaProdutoComboBox->setModel(modal_categorias);

        //get GUI values
        _nome_produto = ui->NomeProduto->toPlainText();

        qry->clear();

        qry->prepare("SELECT * FROM produtos WHERE nome = :in_nome_produto");
        qry->bindValue(":in_nome_produto", _nome_produto);

        if (!qry->exec()){
            qDebug() << "Query report: " << qry->lastError().text() << endl;
        }

        if(qry->size() > 0){
            foundMatch = true;
            ui->alertNomeProduto->show();
            latch = true;
            //found a product, fetc remaining information about it
            QString _cod_produto, _preco, _quantidade, _cod_fabricante, _fabricante, _cod_categoria, _descr_categoria;

            qry->next();

            _cod_produto = qry->value(0).toString();
            _cod_fabricante = qry->value(2).toString();
            _cod_categoria = qry->value(3).toString();
            _quantidade = qry->value(4).toString();
            _preco = qry->value(5).toString();

            //fetch fabricante and _descr_categoria
            qry->clear();
            qry->prepare("SELECT nome FROM fabricantes WHERE cod_fabricante = :in_cod_fabricante");
            qry->bindValue(":in_cod_fabricante", _cod_fabricante.toInt());
            if (!qry->exec()){
                qDebug() << "Query report: " << qry->lastError().text() << endl;
            }
            qry->next();
            _fabricante = qry->value(0).toString();

            qry->clear();
            qry->prepare("SELECT descricao FROM categorias WHERE categoria = :in_cod_categoria");
            qry->bindValue(":in_cod_categoria", _cod_categoria.toInt());
            if (!qry->exec()){
                qDebug() << "Query report: " << qry->lastError().text() << endl;
            }
            qry->next();
            _descr_categoria = qry->value(0).toString();

            latch = true;

            //clear current values
            ui->CodigoProduto->clear();
            ui->precoProduto->clear();
            ui->QuantidadeProduto->clear();

            latch = true;

            ui->CodigoProduto->appendPlainText(_cod_produto);
            ui->precoProduto->appendPlainText(_preco);
            ui->QuantidadeProduto->appendPlainText(_quantidade);
            ui->FabricanteProdutoComboBox->setCurrentText(_fabricante);
            ui->CategoriaProdutoComboBox->setCurrentText(_descr_categoria);

            return;
        } else {
            foundMatch = false;
            ui->alertNomeProduto->hide();
            latch = true;

            ui->CodigoProduto->clear();
            ui->precoProduto->clear();
            ui->QuantidadeProduto->clear();

            //fill in fabricantes combobox
            qry->prepare("SELECT nome FROM fabricantes");

            if (!qry->exec()){
                qDebug() << "Query report: " << qry->lastError().text() << endl;
            }

            modal_fabricantes->setQuery(*qry);

            ui->FabricanteProdutoComboBox->setModel(modal_fabricantes);

            //fill in categorias combobox
            qry->prepare("SELECT descricao FROM categorias");

            if (!qry->exec()){
                qDebug() << "Query report: " << qry->lastError().text() << endl;
            }

            modal_categorias->setQuery(*qry);

            ui->CategoriaProdutoComboBox->setModel(modal_categorias);

            return;
        }
    }
}

void insertProdutos::on_CodigoProduto_textChanged()
{
    qDebug() << "@on_CodigoProduto_textChanged()" << endl;
    if(latch){  //to prevent infinite looping between on_CodigoProduto_textChanged() and on_NomeProduto_textChanged() EventHandlers
        latch = false;

        return;
    } else {
        QString _cod_produto;
        QSqlQuery* qry = new QSqlQuery;

        QSqlQueryModel* modal_categorias = new QSqlQueryModel;
        QSqlQueryModel* modal_fabricantes = new QSqlQueryModel;

        //fill in fabricantes combobox
        qry->prepare("SELECT nome FROM fabricantes");

        if (!qry->exec()){
            qDebug() << "Query report: " << qry->lastError().text() << endl;
        }

        modal_fabricantes->setQuery(*qry);

        ui->FabricanteProdutoComboBox->setModel(modal_fabricantes);

        //fill in categorias combobox
        qry->prepare("SELECT descricao FROM categorias");

        if (!qry->exec()){
            qDebug() << "Query report: " << qry->lastError().text() << endl;
        }

        modal_categorias->setQuery(*qry);

        ui->CategoriaProdutoComboBox->setModel(modal_categorias);

        //get GUI values
        _cod_produto = ui->CodigoProduto->toPlainText();

        qry->clear();

        qry->prepare("SELECT * FROM produtos WHERE cod_produto = :in_cod_produto");
        qry->bindValue(":in_cod_produto", _cod_produto.toInt());

        if (!qry->exec()){
            qDebug() << "Query report: " << qry->lastError().text() << endl;
        }

        if(qry->size() > 0){
            foundMatch = true;
            ui->alertCodigoProduto->show();
            latch = true;
            //found a product, fetc remaining information about it
            QString _nome_produto, _preco, _quantidade, _cod_fabricante, _fabricante, _cod_categoria, _descr_categoria;

            qry->next();

            _nome_produto = qry->value(1).toString();
            _cod_fabricante = qry->value(2).toString();
            _cod_categoria = qry->value(3).toString();
            _quantidade = qry->value(4).toString();
            _preco = qry->value(5).toString();

            //fetch fabricante and _descr_categoria
            qry->clear();
            qry->prepare("SELECT nome FROM fabricantes WHERE cod_fabricante = :in_cod_fabricante");
            qry->bindValue(":in_cod_fabricante", _cod_fabricante.toInt());
            if (!qry->exec()){
                qDebug() << "Query report: " << qry->lastError().text() << endl;
            }
            qry->next();
            _fabricante = qry->value(0).toString();

            qry->clear();
            qry->prepare("SELECT descricao FROM categorias WHERE categoria = :in_cod_categoria");
            qry->bindValue(":in_cod_categoria", _cod_categoria.toInt());
            if (!qry->exec()){
                qDebug() << "Query report: " << qry->lastError().text() << endl;
            }
            qry->next();
            _descr_categoria = qry->value(0).toString();

            latch = true;

            //clear current values
            ui->NomeProduto->clear();
            ui->precoProduto->clear();
            ui->QuantidadeProduto->clear();

            latch = true;

            ui->NomeProduto->appendPlainText(_nome_produto);
            ui->precoProduto->appendPlainText(_preco);
            ui->QuantidadeProduto->appendPlainText(_quantidade);
            ui->FabricanteProdutoComboBox->setCurrentText(_fabricante);
            ui->CategoriaProdutoComboBox->setCurrentText(_descr_categoria);

            return;
        } else {
            foundMatch = false;
            ui->alertCodigoProduto->hide();
            latch = true;

            ui->NomeProduto->clear();
            ui->precoProduto->clear();
            ui->QuantidadeProduto->clear();

            //fill in fabricantes combobox
            qry->prepare("SELECT nome FROM fabricantes");

            if (!qry->exec()){
                qDebug() << "Query report: " << qry->lastError().text() << endl;
            }

            modal_fabricantes->setQuery(*qry);

            ui->FabricanteProdutoComboBox->setModel(modal_fabricantes);

            //fill in categorias combobox
            qry->prepare("SELECT descricao FROM categorias");

            if (!qry->exec()){
                qDebug() << "Query report: " << qry->lastError().text() << endl;
            }

            modal_categorias->setQuery(*qry);

            ui->CategoriaProdutoComboBox->setModel(modal_categorias);

            return;
        }
    }
}
