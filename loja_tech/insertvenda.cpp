#include "insertvenda.h"
#include "ui_insertvenda.h"

insertVenda::insertVenda(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::insertVenda)
{
    ui->setupUi(this);
    ui->PrecoTotal->isReadOnly();
    ui->alertNomeClienteEncontrado->hide();
    ui->alertProdutoEncontrado->hide();
    ui->alertQuantidade->hide();
    ui->PrecoTotal->isReadOnly();
    ui->Tabela->setSortingEnabled(false);
}

insertVenda::~insertVenda()
{
    delete ui;
}

bool insertVenda::setDatabase(QSqlDatabase* database)
{
    db = database;

    if(db->isOpen()){
        return 1;
    } else {
        return 0;
    }
}

void insertVenda::on_insertVendaButton_accepted()
{
    if(ui->Tabela->rowCount() == 0 || stock_less_than_required){
        return;
    }

    QString _cod_cliente, _cod_produto, _quantidade, _quantidade_stock, _preco, _preco_total;
    QSqlQuery* qry = new QSqlQuery;

    _cod_cliente = ui->CodigoCliente->toPlainText();
    _preco_total = ui->PrecoTotal->toPlainText();
    _quantidade = ui->Quantidade->toPlainText();
    _preco_total = ui->PrecoTotal->toPlainText();

    if(_cod_cliente.isEmpty() || _preco_total.isEmpty() || _quantidade.isEmpty() || _preco_total.isEmpty()){
        qDebug() << "_cod_cliente.isEmpty(): " << _cod_cliente.isEmpty() << endl;
        qDebug() << "_preco_total.isEmpty(): " << _preco_total.isEmpty() << endl;
        qDebug() << "_quantidade.isEmpty(): " << _quantidade.isEmpty() << endl;
        qDebug() << "_preco_total.isEmpty(): " << _preco_total.isEmpty() << endl;
        qDebug() << "Some of the fields are empty!" << endl;

        return;
    }

    qDebug() << "Now inserting new sale" << endl;
    qry->clear();
    qry->prepare("call InserirVenda(:in_num_cliente, :in_preco_total)");
    qry->bindValue(":in_num_cliente", _cod_cliente.toInt());
    qry->bindValue(":in_preco_total", _preco_total.toInt());
    if (!qry->exec()){
        qDebug() << "Query report: " << qry->lastError().text() << endl;
    }
    qry->clear();

    //iterar entre todos os tupolos da tabela e inserir cada um em Linhas Venda
    int i = 0;
    while(i < ui->Tabela->rowCount()){
        qDebug() << "From table: " << ui->Tabela->item(i,0)->text() << endl;
        _cod_produto = ui->Tabela->item(i,0)->text();
        _quantidade_stock = ui->Tabela->item(i,2)->text();
        _quantidade = ui->Tabela->item(i,3)->text();
        _preco = ui->Tabela->item(i,4)->text();

        if(_cod_produto.isEmpty() || _quantidade_stock.isEmpty() || _quantidade.isEmpty() || _preco.isEmpty() || _quantidade_stock.isEmpty() < _quantidade.isEmpty()){
            qDebug() << "_cod_produto.isEmpty(): " << _cod_produto.isEmpty() << endl;
            qDebug() << "_quantidade_stock.isEmpty(): " << _quantidade_stock.isEmpty() << endl;
            qDebug() << "_quantidade.isEmpty(): " << _quantidade.isEmpty() << endl;
            qDebug() << "_preco.isEmpty(): " << _preco.isEmpty() << endl;
            qDebug() << "_quantidade_stock.toInt(): " << _quantidade_stock.toInt() << endl;
            qDebug() << "_quantidade.toInt(): " << _quantidade.toInt() << endl;
            qDebug() << "Some of the fields are empty!: " << endl;

            //reset venda
            qry->clear();
            qry->prepare("call removerUltimaVenda()");
            if (!qry->exec()){
                qDebug() << "Query report: " << qry->lastError().text() << endl;
            }

            return;
        }

        qDebug() << "Now inserting sale lines" << endl;
        qry->clear();
        qry->prepare("call InserirLinhasVenda(:in_cod_produto, :in_quantidade, :in_preco)");
        qry->bindValue(":in_cod_produto", _cod_produto);
        qry->bindValue(":in_quantidade", _quantidade);
        qry->bindValue(":in_preco", _preco);
        if (!qry->exec()){
            qDebug() << "Query report: " << qry->lastError().text() << endl;
        }

        i++;
    }

    return;
}

void insertVenda::on_insertVendaButton_rejected()
{
    close();

    return;
}

void insertVenda::on_CodigoCliente_textChanged()
{
    QString _codCliente;
    QSqlQuery* qry = new QSqlQuery;

    _codCliente = ui->CodigoCliente->toPlainText();
    qDebug() << "Insert NomeCliente is: " << _codCliente << endl;

    qry->prepare("SELECT * FROM clientes WHERE num_cliente = :in_cod_cliente");
    qry->bindValue(":in_cod_cliente", _codCliente);

    if (!qry->exec()){
        qDebug() << "Query report: " << qry->lastError().text() << endl;
    }

    if(qry->size() > 0){
        foundMatchCliente = true;
        qDebug() << "foundMatchCliente changed to: " << foundMatchCliente << endl;
        ui->alertNomeClienteEncontrado->show();

        return;
    } else {
        foundMatchCliente = false;
        qDebug() << "foundMatchCliente changed to: " << foundMatchCliente << endl;
        ui->alertNomeClienteEncontrado->hide();
        ui->Quantidade->clear();

        return;
    }
}

void insertVenda::on_CodigoProduto_textChanged()
{
    QString _CodProduto, _quantidade;
    QSqlQuery* qry = new QSqlQuery;

    _CodProduto = ui->CodigoProduto->toPlainText();
    qDebug() << "Insert CodProduto is: " << _CodProduto << endl;

    qry->prepare("SELECT quantidade FROM produtos WHERE cod_produto = :in_cod_produto");
    qry->bindValue(":in_cod_produto", _CodProduto.toInt());

    if (!qry->exec()){
        qDebug() << "Query report: " << qry->lastError().text() << endl;
    }

    if(qry->size() > 0){
        foundMatchProduto = true;
        //_quantidade = qry->value(0).toString();
        qDebug() << "foundMatchProduto changed to: " << foundMatchProduto << endl;
        //ui->alertProdutoEncontrado->show();

        return;
    } else {
        foundMatchProduto = false;
        qDebug() << "foundMatchProduto changed to: " << foundMatchProduto << endl;
        ui->alertProdutoEncontrado->hide();
        ui->Quantidade->clear();

        return;
    }
}

void insertVenda::on_Adicionar_clicked()
{
    if(foundMatchProduto && !stock_less_than_required){
        QString _cod_cliente, _quantidade, _quantidade_stock, _nome_produto, _cod_produto, _preco, _preco_total;
        QSqlQuery* qry = new QSqlQuery;

        //fetch data from GUI
        _cod_cliente = ui->CodigoCliente->toPlainText();
        _cod_produto = ui->CodigoProduto->toPlainText();
        _quantidade = ui->Quantidade->toPlainText();

        //fetch remaining information on product
        qry->clear();
        qry->prepare("SELECT nome, quantidade, preco FROM produtos WHERE cod_produto = :in_cod_produto");
        qry->bindValue(":in_cod_produto", _cod_produto);
        if (!qry->exec()){
            qDebug() << "Query report: " << qry->lastError().text() << endl;
        }
        qry->next();
        _nome_produto = qry->value(0).toString();
        _quantidade_stock = qry->value(1).toString();
        _preco = qry->value(2).toString();

        _preco_total = ui->PrecoTotal->toPlainText();
        _preco_total = QString::number(_preco_total.toInt() + _preco.toInt());
        ui->PrecoTotal->clear();
        ui->PrecoTotal->appendPlainText(_preco_total);

        //insert values into table
        qDebug() << "Row count before new row insertion" << ui->Tabela->rowCount() << endl;
        ui->Tabela->insertRow(ui->Tabela->rowCount());  //insert a new, empty row, at the last index of the table
        qDebug() << "Row count after new row insertion" << ui->Tabela->rowCount() << endl;
        ui->Tabela->setItem(ui->Tabela->rowCount() - 1, 0, new QTableWidgetItem(_cod_produto));
        ui->Tabela->setItem(ui->Tabela->rowCount() - 1, 1, new QTableWidgetItem(_nome_produto));
        ui->Tabela->setItem(ui->Tabela->rowCount() - 1, 2, new QTableWidgetItem(_quantidade));
        ui->Tabela->setItem(ui->Tabela->rowCount() - 1, 3, new QTableWidgetItem(_quantidade_stock));
        ui->Tabela->setItem(ui->Tabela->rowCount() - 1, 4, new QTableWidgetItem(_preco));
    } else {
        qDebug() << "Product will not be inserted!" << endl;

        return;
    }
}

void insertVenda::on_Remover_clicked()
{
    qDebug() << "Now removing a product" << endl;
    QString _cod_produto = ui->CodigoProduto->toPlainText();
    int i = 0;

    qDebug() << "Current row count: " << ui->Tabela->rowCount() << endl;
    if(ui->Tabela->rowCount() == 0){
        qDebug() << "Table is empty" << endl;

        return;
    }

    qDebug() << "Now iterating through rows..." << endl;
    //iterate through indexes to remove item
    while(i < ui->Tabela->rowCount()){
        qDebug() << "Row: " << ui->Tabela->item(i,0)->text() << endl;
        qDebug() << "Current iteration index:" << i << endl;
        if(_cod_produto == (ui->Tabela->item(i,0)->text())){
            QString _preco = ui->Tabela->item(i, 4)->text();
            if(_preco.isEmpty()){
                qDebug() << "Price is empty, will now return and ignore action" << endl;

                return;
            }
            QString _preco_total = ui->PrecoTotal->toPlainText();
            _preco_total = QString::number(_preco_total.toInt() - _preco.toInt());
            ui->PrecoTotal->clear();
            ui->PrecoTotal->appendPlainText(_preco_total);
            ui->Tabela->removeRow(i);

            break;
        }
        i++;
    }
    qDebug() << "Reached end of loop search" << endl;

    return;
}

void insertVenda::on_Quantidade_textChanged()
{
    QString _quantidade, _quantidade_stock, _codProduto;
    QSqlQuery* qry = new QSqlQuery;

    _codProduto = ui->CodigoProduto->toPlainText();
    _quantidade = ui->Quantidade->toPlainText();
    qDebug() << "Insert Quantidade is: " << _quantidade << endl;

    qry->clear();

    qry->prepare("SELECT quantidade FROM produtos WHERE cod_produto = :in_cod_produto");
    qry->bindValue(":in_cod_produto", _codProduto.toInt());

    if (!qry->exec()){
        qDebug() << "Query report: " << qry->lastError().text() << endl;
    }

    if(qry->size() > 0){
        qDebug() << "foundMatchCliente changed to: " << foundMatchCliente << endl;

        qry->next();

        _quantidade_stock = qry->value(0).toString();

        if(_quantidade_stock.toInt() < _quantidade.toInt()){
            stock_less_than_required = true;
            ui->alertQuantidade->show();
        } else {
            stock_less_than_required = false;
            ui->alertQuantidade->hide();
        }
    }

    return;
}
