#-------------------------------------------------
#
# Project created by QtCreator 2016-05-26T13:37:38
#
#-------------------------------------------------

QT       += core
QT       -= gui
QT       += sql
QT       += network
QT       += widgets
CONFIG   += console
CONFIG   -= app_bundle

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = loja_tech
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    insertprodutos.cpp \
    insertvenda.cpp \
    insertcliente.cpp \
    startupdialog.cpp \
    insertfabricante.cpp \
    insertcategorias.cpp \
    greenlight.cpp

HEADERS  += mainwindow.h \
    mysql.h \
    insertprodutos.h \
    insertvenda.h \
    insertcliente.h \
    startupdialog.h \
    insertfabricante.h \
    insertcategorias.h \
    greenlight.h

FORMS    += mainwindow.ui \
    insertprodutos.ui \
    insertvenda.ui \
    insertcliente.ui \
    startupdialog.ui \
    insertfabricante.ui \
    insertcategorias.ui \
    greenlight.ui
