#include <QCoreApplication>
#include <QApplication>
#include <QDebug>
#include <QDialog>
#include <QtGui>
#include <QWidget>
#include <QTabWidget>
#include "mainwindow.h"
#include "startupdialog.h"
#include "mysql.h"


QSqlDatabase openConnection (startupDialog*);


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    startupDialog* startupWindow = new startupDialog;

    QSqlDatabase database = openConnection(startupWindow);
    QSqlDatabase* pdatabase = &database;    //create pointer to database object to be used on window modules

    MainWindow window;

    window.setWindowTitle(QString::fromUtf8("Tech Store"));
    window.setDatabase(pdatabase);
    window.show();
    startupWindow->raise();  //overlap startup echo window over the main one

    return app.exec();
}

QSqlDatabase openConnection (startupDialog* startupWindow)
{

    startupWindow->setWindowTitle(QString::fromUtf8(LOADING));

    //open mysql connection
    qDebug() << LOADING << QCoreApplication::libraryPaths();
    startupWindow->outputLine(LOADING);
    startupWindow->show();

    QSqlDatabase db = QSqlDatabase::addDatabase(DATABASE_TYPE);
    db.addDatabase(DATABASE_TYPE, DATABASE_NAME);
    db.setHostName(HOST);
    db.setDatabaseName(DATABASE);
    db.setUserName(USER);
    db.setPassword(PASSWORD);
    db.setPort(PORT);

    //test connection
    if (db.open()) {
        qDebug() << CONNECTED << endl;
        startupWindow->outputLine(CONNECTED);
    } else {
        qDebug() << CONNERROR << endl << DISCONNECTED << endl;
        startupWindow->outputLine(CONNERROR);
        startupWindow->outputLine(DISCONNECTED);
        startupWindow->~startupDialog();
        db.close();
        db.removeDatabase(DATABASE_NAME);
        exit(1);
    }

    qDebug() << "Is connection open?" << db.isOpen();

    return db;
}
