#ifndef INSERTFABRICANTE_H
#define INSERTFABRICANTE_H

#include <QDialog>
#include <QtSql>
#include "mysql.h"
#include "greenlight.h"

namespace Ui {
class insertFabricante;
}

class insertFabricante : public QDialog
{
    Q_OBJECT

public:
    explicit insertFabricante(QWidget *parent = 0);
    ~insertFabricante();
    bool setDatabase(QSqlDatabase*);

private slots:
    void on_insertFabricanteButton_accepted();

    void on_insertFabricanteButton_rejected();

    void on_NomeFabricante_textChanged();

private:
    Ui::insertFabricante* ui;
    QSqlDatabase* db;
    bool foundMatch = false;
};

#endif // INSERTFABRICANTE_H
