#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QTabWidget>
#include <QDialog>
#include <QtSql>
#include "insertcliente.h"
#include "insertprodutos.h"
#include "insertvenda.h"
#include "insertfabricante.h"
#include "insertcategorias.h"
#include "mysql.h"

#define     CONNECTED   "Successfully connected to database!"
#define     DISCONNECTED    "Not connected!"
#define     CONNERROR    "Database error occurred."
#define     LOADING     "Plugins loaded... "


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    bool setDatabase(QSqlDatabase*);

private slots:
    void on_buttonInserirProdutos_clicked();

    void on_buttonInserirVendas_clicked();

    void on_buttonInserirCliente_clicked();

    void on_insertFabricante_clicked();

    void on_inserirCategorias_clicked();

    void on_searchProdutos_textChanged();

    void on_searchVendas_textChanged();

    void on_searchClientes_textChanged();

    void on_Tabelas_tabBarClicked(int index);

    void on_searchProdutosComboBox_currentIndexChanged(int index);

    void on_searchVendasComboBox_currentIndexChanged(int index);

    void on_searchClientesComboBox_currentIndexChanged(int index);

private:
    Ui::MainWindow* ui;
    QSqlDatabase* db;
};

#endif // MAINWINDOW_H
