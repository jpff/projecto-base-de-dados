#include "mainwindow.h"
#include "ui_mainwindow.h"

void loadProdutos(void);
void loadVendas(void);
void loadClientes(void);

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    db->close();
    db->removeDatabase(DATABASE_NAME);

    delete ui;
}

bool MainWindow::setDatabase(QSqlDatabase* database)
{
    db = database;

    if(db->isOpen()){
        return 1;
    } else {
        return 0;
    }
}

void MainWindow::on_buttonInserirProdutos_clicked()
{
    qDebug() << "Is connection now open for inserirProdutos?" << db->isOpen();

    insertProdutos insertProdutosWindow;
    insertProdutosWindow.setModal(true);
    insertProdutosWindow.setDatabase(db);

    if(insertProdutosWindow.setDatabase(db)){
        qDebug() << "Database is open for subwindow!" << endl;
    } else {
        qDebug() << "Database failed to open for subwindow!" << endl << "Error!" << endl;
        insertProdutosWindow.close();
        return;
    }

    insertProdutosWindow.exec();    //open new subwindow

    return;
}

void MainWindow::on_buttonInserirVendas_clicked()
{
    qDebug() << "Is connection now open for inserirVendas?" << db->isOpen();

    insertVenda insertVendasWindow;
    insertVendasWindow.setModal(true);
    insertVendasWindow.setDatabase(db);

    if(insertVendasWindow.setDatabase(db)){
        qDebug() << "Database is open for subwindow!" << endl;
    } else {
        qDebug() << "Database failed to open for subwindow!" << endl << "Error!" << endl;
        insertVendasWindow.close();
        return;
    }

    insertVendasWindow.exec();  //open new subwindow

    return;
}

void MainWindow::on_buttonInserirCliente_clicked()
{
    qDebug() << "Is connection now open for inserirCliente?" << db->isOpen();

    insertCliente insertClientesWindow;
    insertClientesWindow.setModal(true);
    insertClientesWindow.setDatabase(db);

    if(insertClientesWindow.setDatabase(db)){
        qDebug() << "Database is open for subwindow!" << endl;
    } else {
        qDebug() << "Database failed to open for subwindow!" << endl << "Error!" << endl;
        insertClientesWindow.close();
        return;
    }

    insertClientesWindow.exec();    //open new subwindow

    return;
}

void MainWindow::on_insertFabricante_clicked()
{
    qDebug() << "Is connection now open for inserirFabricante?" << db->isOpen();

    insertFabricante insertFabricanteWindow;
    insertFabricanteWindow.setModal(true);
    insertFabricanteWindow.setDatabase(db);
    if(insertFabricanteWindow.setDatabase(db)){
        qDebug() << "Database is open for subwindow!" << endl;
    } else {
        qDebug() << "Database failed to open for subwindow!" << endl << "Error!" << endl;
        insertFabricanteWindow.close();
        return;
    }
    insertFabricanteWindow.exec();  //open new subwindow

    return;
}

void MainWindow::on_inserirCategorias_clicked()
{
    qDebug() << "Is connection now open for inserirCategorias?" << db->isOpen();

    insertCategorias insertCategoriaWindow;
    insertCategoriaWindow.setModal(true);
    insertCategoriaWindow.setDatabase(db);

    if(insertCategoriaWindow.setDatabase(db)){
        qDebug() << "Database is open for subwindow!" << endl;
    } else {
        qDebug() << "Database failed to open for subwindow!" << endl << "Error!" << endl;
        insertCategoriaWindow.close();

        return;
    }

    insertCategoriaWindow.exec();  //open new subwindow

    return;
}

void MainWindow::on_searchProdutos_textChanged()
{
    qDebug() << "Is connection now open for searchProdutos?" << db->isOpen();

    QString _textInput;
    QSqlQuery* qry = new QSqlQuery(DATABASE_NAME);
    QSqlQueryModel* modal = new QSqlQueryModel;

    _textInput = ui->searchProdutos->toPlainText();

    qDebug() << "Entered value was: " << _textInput << endl;

    if(!_textInput.isEmpty()){
        qDebug() << "Value of _textInput.isEmpty() is: " << _textInput.isEmpty() << endl;
        switch(ui->searchProdutosComboBox->currentIndex()){
        case 0:{
            qry->prepare("select P.cod_produto, P.nome, F.nome, C.descricao, P.quantidade, P.preco from produtos P, fabricantes F, categorias C where P.nome = :in_nome AND P.cod_fabricante = F.cod_fabricante AND P.categoria = C.categoria");
            qry->bindValue(":in_nome", _textInput);
        }
            break;
        case 1:{
            qry->prepare("select P.cod_produto, P.nome, F.nome, C.descricao, P.quantidade, P.preco from produtos P, fabricantes F, categorias C where cod_produto = :in_cod_produto AND P.cod_fabricante = F.cod_fabricante AND P.categoria = C.categoria");
            qry->bindValue(":in_cod_produto", _textInput.toInt());
        }
            break;
        case 2:{
            qry->prepare("select P.cod_produto, P.nome, F.nome, C.descricao, P.quantidade, P.preco from produtos P, fabricantes F, categorias C where cod_fabricante = :in_cod_fabricante AND P.cod_fabricante = F.cod_fabricante AND P.categoria = C.categoria");
            qry->bindValue(":in_cod_fabricante", _textInput.toInt());
        }
            break;
        case 3:{
            qry->prepare("select P.cod_produto, P.nome, F.nome, C.descricao, P.quantidade, P.preco from produtos P, fabricantes F, categorias C where categoria = :in_categoria AND P.cod_fabricante = F.cod_fabricante AND P.categoria = C.categoria");
            qry->bindValue(":in_categoria", _textInput.toInt());
        }
            break;
        case 4:{
            qry->prepare("select P.cod_produto, P.nome, F.nome, C.descricao, P.quantidade, P.preco from produtos P, fabricantes F, categorias C where quantidade = :in_quantidade AND P.cod_fabricante = F.cod_fabricante AND P.categoria = C.categoria");
            qry->bindValue(":in_quantidade", _textInput.toInt());
        }
            break;
        }
    } else {
        qry->prepare("select P.cod_produto, P.nome, F.nome, C.descricao, P.quantidade, P.preco from produtos P, fabricantes F, categorias C where P.cod_fabricante = F.cod_fabricante AND P.categoria = C.categoria");
    }

    qDebug() << "Executing search query!" << endl;
    if (!qry->exec()){
        qDebug() << "Query report: " << qry->lastError().text() << endl;
    }

    modal->setQuery(*qry);
    modal->setHeaderData(0, Qt::Horizontal, QObject::tr("Codigo Produto"));
    modal->setHeaderData(1, Qt::Horizontal, QObject::tr("Nome"));
    modal->setHeaderData(2, Qt::Horizontal, QObject::tr("Fabricante"));
    modal->setHeaderData(3, Qt::Horizontal, QObject::tr("Categoria"));
    modal->setHeaderData(4, Qt::Horizontal, QObject::tr("Quantidade"));
    modal->setHeaderData(5, Qt::Horizontal, QObject::tr("Preco"));

    ui->listProdutos->setModel(modal);
    ui->listProdutos->NoEditTriggers;   //displayed list is read-only now
    ui->listProdutos->show();

    qDebug() << "Found" << modal->rowCount() << "entries!";

    return;
}

void MainWindow::on_searchVendas_textChanged()
{
    qDebug() << "Is connection open for searchVendas?" << db->isOpen();

    QString _textInput;
    QSqlQuery* qry = new QSqlQuery(DATABASE_NAME);
    QSqlQueryModel* modal = new QSqlQueryModel;

    _textInput = ui->searchVendas->toPlainText();

    qDebug() << "Entered value was: " << _textInput << endl;

    qDebug() << "Value of _textInput.isEmpty() is: " << _textInput.isEmpty() << endl;
    switch(ui->searchVendasComboBox->currentIndex()){
    case 0:{
        if(!_textInput.isEmpty()){
        qry->prepare("select V.num_venda, C.num_cliente, C.nome, V.data_venda, V.preco_total from clientes C, vendas V where V.num_venda = :in_num_venda AND V.num_cliente = C.num_cliente");
        qry->bindValue(":in_num_venda", _textInput.toInt());
        } else {
            qry->prepare("select V.num_venda, C.num_cliente, C.nome, V.data_venda, V.preco_total from clientes C, vendas V where V.num_cliente = C.num_cliente");
        }
    }
        break;
    case 1:{
        if(!_textInput.isEmpty()){
            qry->prepare("select V.num_venda, C.num_cliente, C.nome, V.data_venda, V.preco_total from clientes C, vendas V where V.num_cliente = :in_num_cliente AND V.num_cliente = C.num_cliente");
            qry->bindValue(":in_num_cliente", _textInput.toInt());
        } else {
            qry->prepare("select V.num_venda, C.num_cliente, C.nome, V.data_venda, V.preco_total from clientes C, vendas V where V.num_cliente = C.num_cliente");
        }
    }
        break;
    case 2:{
        if(!_textInput.isEmpty()){
            qry->prepare("select V.num_venda, C.num_cliente, C.nome, V.data_venda, V.preco_total from clientes C, vendas V where V.data_venda = :in_data_venda AND V.num_cliente = C.num_cliente");
            qry->bindValue(":in_data_venda", _textInput);
        } else {
            qry->prepare("select V.num_venda, C.num_cliente, C.nome, V.data_venda, V.preco_total from clientes C, vendas V where V.num_cliente = C.num_cliente");
        }
    }
        break;
    case 3:{
        if(!_textInput.isEmpty()){
            qry->prepare("select V.num_venda, C.num_cliente, C.nome, V.data_venda, V.preco_total from clientes C, vendas V where V.preco_total = :in_preco_total AND V.num_cliente = C.num_cliente");
            qry->bindValue(":in_preco_total", _textInput.toInt());
        } else {
            qry->prepare("select V.num_venda, C.num_cliente, C.nome, V.data_venda, V.preco_total from clientes C, vendas V where V.num_cliente = C.num_cliente");
        }
    }
        break;
    case 4:{
        if(!_textInput.isEmpty()){  //if not empty
            qry->prepare("select V.num_venda, LV.cod_produto, P.nome, LV.quantidade, LV.preÃ§o from vendas V, linhas_venda LV, produtos P where V.num_venda = :in_num_venda AND V.num_venda = LV.num_venda AND LV.cod_produto = P.cod_produto");
            qry->bindValue(":in_num_venda", _textInput.toInt());

            qry->exec();

            modal->setQuery(*qry);
            modal->setHeaderData(0, Qt::Horizontal, QObject::tr("Numero Venda"));
            modal->setHeaderData(1, Qt::Horizontal, QObject::tr("Codigo Produto"));
            modal->setHeaderData(2, Qt::Horizontal, QObject::tr("Nome Produto"));
            modal->setHeaderData(3, Qt::Horizontal, QObject::tr("Quantidade"));
            modal->setHeaderData(4, Qt::Horizontal, QObject::tr("PreÃ§o"));

            ui->listVendas->setModel(modal);
            ui->listVendas->NoEditTriggers;   //displayed list is read-only now
            ui->listVendas->show();

                qDebug() << modal->rowCount();

                return;
        } else {    //if empty
            qry->prepare("select V.num_venda, LV.cod_produto, P.nome, LV.quantidade, LV.preÃ§o from vendas V, linhas_venda LV, produtos P where V.num_venda = LV.num_venda AND LV.cod_produto = P.cod_produto");

            qry->exec();

            modal->setQuery(*qry);
            modal->setHeaderData(0, Qt::Horizontal, QObject::tr("Numero Venda"));
            modal->setHeaderData(1, Qt::Horizontal, QObject::tr("Codigo Produto"));
            modal->setHeaderData(2, Qt::Horizontal, QObject::tr("Nome Produto"));
            modal->setHeaderData(3, Qt::Horizontal, QObject::tr("Quantidade"));
            modal->setHeaderData(4, Qt::Horizontal, QObject::tr("PreÃ§o"));

            ui->listVendas->setModel(modal);
            ui->listVendas->NoEditTriggers;   //displayed list is read-only now
            ui->listVendas->show();

            qDebug() << modal->rowCount();

            return;
        }
    }
            break;
    }

    qry->exec();

    modal->setQuery(*qry);
    modal->setHeaderData(0, Qt::Horizontal, QObject::tr("Numero Venda"));
    modal->setHeaderData(1, Qt::Horizontal, QObject::tr("Codigo Cliente"));
    modal->setHeaderData(2, Qt::Horizontal, QObject::tr("Nome Cliente"));
    modal->setHeaderData(3, Qt::Horizontal, QObject::tr("Data Venda"));
    modal->setHeaderData(4, Qt::Horizontal, QObject::tr("PreÃ§o Total"));

    ui->listVendas->setModel(modal);
    ui->listVendas->NoEditTriggers;   //displayed list is read-only now
    ui->listVendas->show();

    qDebug() << "Found" << modal->rowCount() << "entries!";

    return;
}

void MainWindow::on_searchClientes_textChanged()
{
    qDebug() << "Is connection open for searchClientes?" << db->isOpen();

    QString _textInput;
    QSqlQuery* qry = new QSqlQuery(DATABASE_NAME);
    QSqlQueryModel* modal = new QSqlQueryModel;

    _textInput = ui->searchClientes->toPlainText();

    qDebug() << "Entered value was: " << _textInput << endl;

    if(!_textInput.isEmpty()){
        qDebug() << "Value of _textInput.isEmpty() is: " << _textInput.isEmpty() << endl;
        switch(ui->searchClientesComboBox->currentIndex()){
        case 0:{
            qry->prepare("select num_cliente, nome, morada, pontos, cod_postal from clientes where num_cliente = :in_num_cliente");
            qry->bindValue(":in_num_cliente", _textInput.toInt());
        }
            break;
        case 1:{
            qry->prepare("select num_cliente, nome, morada, pontos, cod_postal from clientes where nome = :in_nome");
            qry->bindValue(":in_nome", _textInput);
        }
            break;
        case 2:{
            qry->prepare("select num_cliente, nome, morada, pontos, cod_postal from clientes where morada = :in_morada");
            qry->bindValue(":in_morada", _textInput);
        }
            break;
        case 3:{
            qry->prepare("select num_cliente, nome, morada, pontos, cod_postal from clientes where pontos = :in_pontos");
            qry->bindValue(":in_pontos", _textInput.toInt());
        }
            break;
        case 4:{
            qry->prepare("select num_cliente, nome, morada, pontos, cod_postal from clientes where cod_postal = :in_cod_postal");
            qry->bindValue(":in_cod_postal", _textInput);
        }
            break;
        }
    } else {
        qry->prepare("select num_cliente, nome, morada, pontos, cod_postal from clientes");
    }

    qry->exec();

    modal->setQuery(*qry);
    modal->setHeaderData(0, Qt::Horizontal, QObject::tr("CÃ³digo Cliente"));
    modal->setHeaderData(1, Qt::Horizontal, QObject::tr("Nome"));
    modal->setHeaderData(2, Qt::Horizontal, QObject::tr("Morada"));
    modal->setHeaderData(3, Qt::Horizontal, QObject::tr("Pontos"));
    modal->setHeaderData(4, Qt::Horizontal, QObject::tr("CÃ³digo Postal"));

    ui->listClientes->setModel(modal);
    ui->listClientes->NoEditTriggers;   //displayed list is read-only now
    ui->listClientes->show();

    qDebug() << "Found" << modal->rowCount() << "entries!";

    return;
}

void MainWindow::on_Tabelas_tabBarClicked(int index)
{
    switch(index){
    case 0:{
        //do nothing, wait for user to select an option
    }
        break;
    case 1:{
        //produtos
        on_searchProdutos_textChanged();
    }
        break;
    case 2:{
        //vendas
        on_searchVendas_textChanged();
    }
        break;
    case 3:{
        //clientes
        on_searchClientes_textChanged();
    }
        break;
    case 4:{
        //statistics
        QString _nomeFabricante;
        QSqlQuery* qry = new QSqlQuery;
        QSqlQueryModel* modalClientes = new QSqlQueryModel;
        QSqlQueryModel* modalDias = new QSqlQueryModel;

        qry->clear();

        //fill in most popular manufacturer
        qry->prepare("SELECT nome FROM FabricanteMaiorSaida");
        if (!qry->exec()){
            qDebug() << "Query report: " << qry->lastError().text() << endl;
        }
        qry->next();
        _nomeFabricante = qry->value(0).toString();
        qDebug() << "exec FabricanteMaiorSaida" << endl;
        ui->FabricanteMaiorSaida->clear();
        ui->FabricanteMaiorSaida->appendPlainText(_nomeFabricante);

        qry->clear();

        qry->prepare("SELECT * FROM TresMelhoresClientes");
        if (!qry->exec()){
            qDebug() << "Query report: " << qry->lastError().text() << endl;
        }
        qry->next();
        modalClientes->setQuery(*qry);
        qDebug() << "exec MelhoresClientes" << endl;
        ui->MelhoresClientes->setModel(modalClientes);
        ui->MelhoresClientes->NoEditTriggers;

        qry->clear();

        qry->prepare("SELECT * FROM TresMelhoresDias");
        if (!qry->exec()){
            qDebug() << "Query report: " << qry->lastError().text() << endl;
        }
        qDebug() << "exec MelhoresDias" << endl;
        modalDias->setQuery(*qry);
        ui->MelhoresDias->setModel(modalDias);
        ui->MelhoresDias->NoEditTriggers;
    }
        break;
    }

    return;
}

void MainWindow::on_searchProdutosComboBox_currentIndexChanged(int index)
{
    qDebug() << "Is connection now open for searchProdutos?" << db->isOpen();

    QString _textInput;
    QSqlQuery* qry = new QSqlQuery(DATABASE_NAME);
    QSqlQueryModel* modal = new QSqlQueryModel;

    _textInput = ui->searchProdutos->toPlainText();

    qDebug() << "Entered value was: " << _textInput << endl;

    if(!_textInput.isEmpty()){
        qDebug() << "Value of _textInput.isEmpty() is: " << _textInput.isEmpty() << endl;
        switch(index){
        case 0:{
            qry->prepare("select P.cod_produto, P.nome, F.nome, C.descricao, P.quantidade from produtos P, fabricantes F, categorias C where P.nome = :in_nome AND P.cod_fabricante = F.cod_fabricante AND P.categoria = C.categoria");
            qry->bindValue(":in_nome", _textInput);
        }
            break;
        case 1:{
            qry->prepare("select P.cod_produto, P.nome, F.nome, C.descricao, P.quantidade from produtos P, fabricantes F, categorias C where cod_produto = :in_cod_produto AND P.cod_fabricante = F.cod_fabricante AND P.categoria = C.categoria");
            qry->bindValue(":in_cod_produto", _textInput.toInt());
        }
            break;
        case 2:{
            qry->prepare("select P.cod_produto, P.nome, F.nome, C.descricao, P.quantidade from produtos P, fabricantes F, categorias C where cod_fabricante = :in_cod_fabricante AND P.cod_fabricante = F.cod_fabricante AND P.categoria = C.categoria");
            qry->bindValue(":in_cod_fabricante", _textInput.toInt());
        }
            break;
        case 3:{
            qry->prepare("select P.cod_produto, P.nome, F.nome, C.descricao, P.quantidade from produtos P, fabricantes F, categorias C where categoria = :in_categoria AND P.cod_fabricante = F.cod_fabricante AND P.categoria = C.categoria");
            qry->bindValue(":in_categoria", _textInput.toInt());
        }
            break;
        case 4:{
            qry->prepare("select P.cod_produto, P.nome, F.nome, C.descricao, P.quantidade from produtos P, fabricantes F, categorias C where quantidade = :in_quantidade AND P.cod_fabricante = F.cod_fabricante AND P.categoria = C.categoria");
            qry->bindValue(":in_quantidade", _textInput.toInt());
        }
            break;
        }
    } else {
        qry->prepare("select P.cod_produto, P.nome, F.nome, C.descricao, P.quantidade from produtos P, fabricantes F, categorias C where P.cod_fabricante = F.cod_fabricante AND P.categoria = C.categoria");
    }

    qry->exec();

    modal->setQuery(*qry);
    modal->setHeaderData(0, Qt::Horizontal, QObject::tr("Codigo Produto"));
    modal->setHeaderData(1, Qt::Horizontal, QObject::tr("Nome"));
    modal->setHeaderData(2, Qt::Horizontal, QObject::tr("Fabricante"));
    modal->setHeaderData(3, Qt::Horizontal, QObject::tr("Categoria"));
    modal->setHeaderData(4, Qt::Horizontal, QObject::tr("Quantidade"));

    ui->listProdutos->setModel(modal);
    ui->listProdutos->NoEditTriggers;   //displayed list is read-only now
    ui->listProdutos->show();

    qDebug() << "Found" << modal->rowCount() << "entries!";

    return;
}

void MainWindow::on_searchVendasComboBox_currentIndexChanged(int index)
{
    qDebug() << "Is connection open for searchVendas?" << db->isOpen();

    QString _textInput;
    QSqlQuery* qry = new QSqlQuery(DATABASE_NAME);
    QSqlQueryModel* modal = new QSqlQueryModel;

    _textInput = ui->searchVendas->toPlainText();

    qDebug() << "Entered value was: " << _textInput << endl;

    qDebug() << "Value of _textInput.isEmpty() is: " << _textInput.isEmpty() << endl;
    switch(index){
    case 0:{
        if(!_textInput.isEmpty()){
        qry->prepare("select V.num_venda, C.num_cliente, C.nome, V.data_venda, V.preco_total from clientes C, vendas V where V.num_venda = :in_num_venda AND V.num_cliente = C.num_cliente");
        qry->bindValue(":in_num_venda", _textInput.toInt());
        } else {
            qry->prepare("select V.num_venda, C.num_cliente, C.nome, V.data_venda, V.preco_total from clientes C, vendas V where V.num_cliente = C.num_cliente");
        }
    }
        break;
    case 1:{
        if(!_textInput.isEmpty()){
            qry->prepare("select V.num_venda, C.num_cliente, C.nome, V.data_venda, V.preco_total from clientes C, vendas V where V.num_cliente = :in_num_cliente AND V.num_cliente = C.num_cliente");
            qry->bindValue(":in_num_cliente", _textInput.toInt());
        } else {
            qry->prepare("select V.num_venda, C.num_cliente, C.nome, V.data_venda, V.preco_total from clientes C, vendas V where V.num_cliente = C.num_cliente");
        }
    }
        break;
    case 2:{
        if(!_textInput.isEmpty()){
            qry->prepare("select V.num_venda, C.num_cliente, C.nome, V.data_venda, V.preco_total from clientes C, vendas V where V.data_venda = :in_data_venda AND V.num_cliente = C.num_cliente");
            qry->bindValue(":in_data_venda", _textInput);
        } else {
            qry->prepare("select V.num_venda, C.num_cliente, C.nome, V.data_venda, V.preco_total from clientes C, vendas V where V.num_cliente = C.num_cliente");
        }
    }
        break;
    case 3:{
        if(!_textInput.isEmpty()){
            qry->prepare("select V.num_venda, C.num_cliente, C.nome, V.data_venda, V.preco_total from clientes C, vendas V where V.preco_total = :in_preco_total AND V.num_cliente = C.num_cliente");
            qry->bindValue(":in_preco_total", _textInput.toInt());
        } else {
            qry->prepare("select V.num_venda, C.num_cliente, C.nome, V.data_venda, V.preco_total from clientes C, vendas V where V.num_cliente = C.num_cliente");
        }
    }
        break;
    case 4:{
        if(!_textInput.isEmpty()){  //if not empty
            qry->prepare("select V.num_venda, LV.cod_produto, P.nome, LV.quantidade, LV.preÃ§o from vendas V, linhas_venda LV, produtos P where V.num_venda = :in_num_venda AND V.num_venda = LV.num_venda AND LV.cod_produto = P.cod_produto");
            qry->bindValue(":in_num_venda", _textInput.toInt());

            qry->exec();

            modal->setQuery(*qry);
            modal->setHeaderData(0, Qt::Horizontal, QObject::tr("Numero Venda"));
            modal->setHeaderData(1, Qt::Horizontal, QObject::tr("Codigo Produto"));
            modal->setHeaderData(2, Qt::Horizontal, QObject::tr("Nome Produto"));
            modal->setHeaderData(3, Qt::Horizontal, QObject::tr("Quantidade"));
            modal->setHeaderData(4, Qt::Horizontal, QObject::tr("PreÃ§o"));

            ui->listVendas->setModel(modal);
            ui->listVendas->NoEditTriggers;   //displayed list is read-only now
            ui->listVendas->show();

                qDebug() << modal->rowCount();

                return;
        } else {    //if empty
            qry->prepare("select V.num_venda, LV.cod_produto, P.nome, LV.quantidade, LV.preÃ§o from vendas V, linhas_venda LV, produtos P where V.num_venda = LV.num_venda AND LV.cod_produto = P.cod_produto");

            qry->exec();

            modal->setQuery(*qry);
            modal->setHeaderData(0, Qt::Horizontal, QObject::tr("Numero Venda"));
            modal->setHeaderData(1, Qt::Horizontal, QObject::tr("Codigo Produto"));
            modal->setHeaderData(2, Qt::Horizontal, QObject::tr("Nome Produto"));
            modal->setHeaderData(3, Qt::Horizontal, QObject::tr("Quantidade"));
            modal->setHeaderData(4, Qt::Horizontal, QObject::tr("PreÃ§o"));

            ui->listVendas->setModel(modal);
            ui->listVendas->NoEditTriggers;   //displayed list is read-only now
            ui->listVendas->show();

            qDebug() << modal->rowCount();

            return;
        }
    }
            break;
    }

    qry->exec();

    modal->setQuery(*qry);
    modal->setHeaderData(0, Qt::Horizontal, QObject::tr("Numero Venda"));
    modal->setHeaderData(1, Qt::Horizontal, QObject::tr("Codigo Cliente"));
    modal->setHeaderData(2, Qt::Horizontal, QObject::tr("Nome Cliente"));
    modal->setHeaderData(3, Qt::Horizontal, QObject::tr("Data Venda"));
    modal->setHeaderData(4, Qt::Horizontal, QObject::tr("PreÃ§o Total"));

    ui->listVendas->setModel(modal);
    ui->listVendas->NoEditTriggers;   //displayed list is read-only now
    ui->listVendas->show();

    qDebug() << "Found" << modal->rowCount() << "entries!";

    return;
}

void MainWindow::on_searchClientesComboBox_currentIndexChanged(int index)
{
    qDebug() << "Is connection open for searchClientes?" << db->isOpen();

    QString _textInput;
    QSqlQuery* qry = new QSqlQuery(DATABASE_NAME);
    QSqlQueryModel* modal = new QSqlQueryModel;

    _textInput = ui->searchClientes->toPlainText();

    qDebug() << "Entered value was: " << _textInput << endl;

    if(!_textInput.isEmpty()){
        qDebug() << "Value of _textInput.isEmpty() is: " << _textInput.isEmpty() << endl;
        switch(index){
        case 0:{
            qry->prepare("select num_cliente, nome, morada, pontos, cod_postal from clientes where num_cliente = :in_num_cliente");
            qry->bindValue(":in_num_cliente", _textInput.toInt());
        }
            break;
        case 1:{
            qry->prepare("select num_cliente, nome, morada, pontos, cod_postal from clientes where nome = :in_nome");
            qry->bindValue(":in_nome", _textInput);
        }
            break;
        case 2:{
            qry->prepare("select num_cliente, nome, morada, pontos, cod_postal from clientes where morada = :in_morada");
            qry->bindValue(":in_morada", _textInput);
        }
            break;
        case 3:{
            qry->prepare("select num_cliente, nome, morada, pontos, cod_postal from clientes where pontos = :in_pontos");
            qry->bindValue(":in_pontos", _textInput.toInt());
        }
            break;
        case 4:{
            qry->prepare("select num_cliente, nome, morada, pontos, cod_postal from clientes where cod_postal = :in_cod_postal");
            qry->bindValue(":in_cod_postal", _textInput);
        }
            break;
        }
    } else {
        qry->prepare("select num_cliente, nome, morada, pontos, cod_postal from clientes");
    }

    qry->exec();

    modal->setQuery(*qry);
    modal->setHeaderData(0, Qt::Horizontal, QObject::tr("CÃ³digo Cliente"));
    modal->setHeaderData(1, Qt::Horizontal, QObject::tr("Nome"));
    modal->setHeaderData(2, Qt::Horizontal, QObject::tr("Morada"));
    modal->setHeaderData(3, Qt::Horizontal, QObject::tr("Pontos"));
    modal->setHeaderData(4, Qt::Horizontal, QObject::tr("CÃ³digo Postal"));

    ui->listClientes->setModel(modal);
    ui->listClientes->NoEditTriggers;   //displayed list is read-only now
    ui->listClientes->show();

    qDebug() << "Found" << modal->rowCount() << "entries!";

    return;
}
