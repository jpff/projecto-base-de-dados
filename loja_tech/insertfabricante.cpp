#include "insertfabricante.h"
#include "ui_insertfabricante.h"

insertFabricante::insertFabricante(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::insertFabricante)
{
    ui->setupUi(this);
    ui->alertFoundMatch->hide();
}

insertFabricante::~insertFabricante()
{
    delete ui;
}

bool insertFabricante::setDatabase(QSqlDatabase* database)
{
    db = database;

    if(db->isOpen()){
        return 1;
    } else {
        return 0;
    }
}

void insertFabricante::on_insertFabricanteButton_accepted()
{
    QString _nome_fabricante, _cod_postal, _localizacao;
    QSqlQuery* qry = new QSqlQuery(DATABASE_NAME);

    qDebug() << "Status of foundMatch is: " << foundMatch << endl;

    if(foundMatch){ //if a match exists, we're updating a client's information
        qDebug() << "An entry for that manufacturer already exists" << endl;

        return;
    } else {
        qDebug() << "Entered INSERTION process" << endl;

        _nome_fabricante = ui->NomeFabricante->toPlainText();
        _cod_postal = ui->CodigoPostalFabricante->toPlainText();
        _localizacao = ui->MoradaFabricante->toPlainText();

        qDebug() << "Current _nome_fabricante is: " << _nome_fabricante << endl;

        ui->NomeFabricante->clear();

        qDebug() << "Preparing query" << endl;
        qry->prepare("INSERT INTO fabricantes (cod_fabricante, nome, localizacao, cod_postal) VALUES (NULL, :in_nome_fabricante, :in_cod_postal, :in_localizacao)");
        qry->bindValue(":in_nome_fabricante", _nome_fabricante);
        qry->bindValue(":in_cod_postal", _cod_postal);
        qry->bindValue(":in_localizacao", _localizacao);

        if (!qry->exec()){
            qDebug() << "Query report: " << qry->lastError().text() << endl;
        }
        qDebug() << "Completed INSERT action" << endl;
    }

    qDebug() << "Reached ending for category insertions" << endl;

    return;
}

void insertFabricante::on_insertFabricanteButton_rejected()
{
    close();

    return;
}

void insertFabricante::on_NomeFabricante_textChanged()
{
    QSqlQuery* qry = new QSqlQuery(DATABASE_NAME);
    QString _nome_fabricante;

    _nome_fabricante = ui->NomeFabricante->toPlainText();  //read input value
    qDebug() << "Inserted value is: " << _nome_fabricante << endl;

    //search records for that value
    qry->prepare("select * from fabricantes where nome = :in_nome_fabricante");
    qry->bindValue(":in_nome_fabricante", _nome_fabricante);
    qry->exec();

    qDebug() << "Possible errors: " << qry->lastError() << endl;
    qDebug() << "Found" << qry->record().count() << "entries." << endl;

    //process entries
    if(qry->size() > 0){ //if found a match then fill in all the other fields for the user
        qDebug() << "Manufacturer already exists!" << endl;
        qDebug() << "Size of query reports: " << qry->size() << endl;
        foundMatch = true;
        ui->alertFoundMatch->show();

        qDebug() << "Categoria found was " << qry->value(1).toString() << endl;
    } else {    //if no match found clear the other fields
        foundMatch = false;
        ui->alertFoundMatch->hide();
    }

    qDebug() << "foundMatch = " << foundMatch << endl;

    return;
}
