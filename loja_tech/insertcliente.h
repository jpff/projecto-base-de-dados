#ifndef INSERTCLIENTE_H
#define INSERTCLIENTE_H

#include <QDialog>
#include <QtSql>
#include "mysql.h"
#include "greenlight.h"

namespace Ui {
class insertCliente;
}

class insertCliente : public QDialog
{
    Q_OBJECT

public:
    explicit insertCliente(QWidget *parent = 0);
    ~insertCliente();
    bool setDatabase(QSqlDatabase*);

private slots:
    void on_insertClienteButton_accepted();

    void on_insertClienteButton_rejected();

    void on_CodigoCliente_textChanged();

private:
    Ui::insertCliente* ui;
    QSqlDatabase* db;
    bool foundMatch = false;
};

#endif // INSERTCLIENTE_H
