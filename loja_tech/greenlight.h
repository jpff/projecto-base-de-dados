#ifndef GREENLIGHT_H
#define GREENLIGHT_H

#include <QDialog>
#include <QDebug>

namespace Ui {
class greenlight;
}

class greenlight : public QDialog
{
    Q_OBJECT

public:
    explicit greenlight(QWidget *parent = 0);
    ~greenlight();
    void setFlag(bool*);

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::greenlight *ui;
    bool* _greenlight;
};

#endif // GREENLIGHT_H
