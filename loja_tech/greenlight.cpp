#include "greenlight.h"
#include "ui_greenlight.h"

greenlight::greenlight(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::greenlight)
{
    ui->setupUi(this);
}

greenlight::~greenlight()
{
    delete ui;
}

void greenlight::setFlag(bool* in_greenlight){
    _greenlight = in_greenlight;
    show();

    return;
}

void greenlight::on_buttonBox_accepted()
{
    *_greenlight = true;
    qDebug() << "Greenlight returned with flag: " << *_greenlight << endl;

    return;
}

void greenlight::on_buttonBox_rejected()
{
    if(*_greenlight){
        *_greenlight = false;
    }
    qDebug() << "Greenlight returned with flag: " << *_greenlight << endl;

    return;
}
