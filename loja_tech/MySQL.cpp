#include "MySQL.h"

QSqlDatabase createConnection(void)
{
    qDebug() << "Plugins loaded from: " << QCoreApplication::libraryPaths();
    QSqlDatabase db = QSqlDatabase::addDatabase(DATABASE_TYPE);
    db.addDatabase(DATABASE_TYPE, DATABASE_NAME);
    db.setHostName(HOST);
    db.setDatabaseName(DATABASE);
    db.setUserName(USER);
    db.setPassword(PASSWORD);
    db.setPort(PORT);

    if (db.open()) {
        qDebug() << "Successfully connected to database!" << endl;
        return db;
    } else {
        qDebug() << "Database error occurred" << endl << "Not connected!" << endl;
        exit(1);
    }
}

void closeConnection(QSqlDatabase db)
{
    if(db.open()) {
        db.close();
    }
    db.~QSqlDatabase();

    return;
}
