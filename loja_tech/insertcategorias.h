#ifndef INSERTCATEGORIAS_H
#define INSERTCATEGORIAS_H

#include <QDialog>
#include <QtSql>
#include "mysql.h"
#include "greenlight.h"

namespace Ui {
class insertCategorias;
}

class insertCategorias : public QDialog
{
    Q_OBJECT

public:
    explicit insertCategorias(QWidget *parent = 0);
    ~insertCategorias();
    bool setDatabase(QSqlDatabase*);

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_descricaoCategoria_textChanged();

private:
    Ui::insertCategorias *ui;
    QSqlDatabase* db;
    bool foundMatch = false;
};

#endif // INSERTCATEGORIAS_H
