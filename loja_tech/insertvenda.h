#ifndef INSERTVENDA_H
#define INSERTVENDA_H

#include <QDialog>
#include <QtSql>
#include "mysql.h"
#include "greenlight.h"

namespace Ui {
class insertVenda;
}

class insertVenda : public QDialog
{
    Q_OBJECT

public:
    explicit insertVenda(QWidget *parent = 0);
    ~insertVenda();
    bool setDatabase(QSqlDatabase*);

private slots:
    void on_insertVendaButton_accepted();

    void on_insertVendaButton_rejected();

    void on_CodigoCliente_textChanged();

    void on_CodigoProduto_textChanged();

    void on_Adicionar_clicked();

    void on_Remover_clicked();

    void on_Quantidade_textChanged();

private:
    Ui::insertVenda* ui;
    QSqlDatabase* db;
    bool foundMatchCliente = false;
    bool foundMatchProduto = false;
    bool stock_less_than_required = false;
};

#endif // INSERTVENDA_H
